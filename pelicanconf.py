#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Pradhvan'
SITENAME = 'Pradhvan Bisht'
DOMAIN = 'pradhvanbisht.in'
BIO_TEXT = 'Python Developer'
PATH = 'content'


TIMEZONE = 'Asia/Kolkata'

DEFAULT_LANG = 'en'

TWITTER_USERNAME = '@bishtpradhvan'
INDEX_DESCRIPTION = 'Website and blog of Pradhvan'

SIDEBAR_LINKS = [
    '<a href="https://pradhvanbisht.in/about.html">About</a>',
    '<a href="/archive/">Archive</a>',
]

ICONS_PATH = 'images/icons'

SOCIAL_ICONS = [
    ('mailto:pradhvanbisht@gmail.com',
     'Email (pradhvanbisht@gmail.com)', 'fa-envelope'),
    ('http://twitter.com/bishtpradhban', 'Twitter', 'fa-twitter'),
    ('http://github.com/pradhvan', 'GitHub', 'fa-github'),
    ('http://soundcloud.com/iKevinY', 'SoundCloud', 'fa-soundcloud'),
    ('/atom.xml', 'Atom Feed', 'fa-rss'),
]




# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True


HEME_COLOR = '#FF8000'

# Pelican settings
RELATIVE_URLS = True
SITEURL = 'https://pradhvanbisht.in'
TIMEZONE = 'Asia/Kolkata'
DEFAULT_DATE = 'fs'
DEFAULT_DATE_FORMAT = '%B %d, %Y'
DEFAULT_PAGINATION = False
SUMMARY_MAX_LENGTH = 42

THEME = 'theme/pneumatic'

ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}/'
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'

PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'

ARCHIVES_SAVE_AS = 'archive/index.html'
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'

# Disable authors, categories, tags, and category pages
DIRECT_TEMPLATES = ['index', 'archives']
CATEGORY_SAVE_AS = ''

# Disable Atom feed generation
FEED_ATOM = 'atom.xml'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

TYPOGRIFY = True

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.admonition': {},
        'markdown.extensions.codehilite': {'linenums': None},
        'markdown.extensions.extra': {},
    },
    'output_format': 'html5',
}

CACHE_CONTENT = False
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_PATH = 'develop'
PATH = 'content'

templates = ['404.html']
TEMPLATE_PAGES = {page: page for page in templates}

STATIC_PATHS = ['images', 'uploads', 'extra']
IGNORE_FILES = ['.DS_Store', 'pneumatic.scss', 'pygments.css']

extras = ['CNAME', 'favicon.ico', 'robots.txt']
EXTRA_PATH_METADATA = {'extra/%s' % file: {'path': file} for file in extras}

PLUGIN_PATHS = ['plugins']
PLUGINS = ['assets', 'neighbors', 'render_math']
ASSET_SOURCE_PATHS = ['static']
ASSET_CONFIG = [
    ('cache', False),
    ('manifest', False),
    ('url_expire', False),
    ('versions', False),
]
